// Return customers who have more undelivered orders than delivered
fun Shop.getCustomersWithMoreUndeliveredOrders(): Set<Customer> = 
    customers.filter{
        var (del, undel) = it.orders.partition{it.isDelivered == true}
        del.size < undel.size
    }.toSet()