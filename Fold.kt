// Return the set of products that were ordered by all customers
fun Shop.getProductsOrderedByAll(): Set<Product> {
    return customers.fold(customers.flatMap{it.getOrderedProducts()}.toSet()){allOrders, customer ->
    	allOrders.intersect(customer.getOrderedProducts())}
}

fun Customer.getOrderedProducts(): List<Product> =
        orders.flatMap{it.products}